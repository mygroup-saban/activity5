// Developer: Donna G. Saban
// Date: 1/13/2023
// Description: Capstone JS OOP

// alert('Capstone JS OOP'); 


class Customer {
    constructor(email) {
        this.email = email; 
        this.cart = new Cart();
        this.orders = [];
    }
    checkOut() {
        if (this.cart.contents.length > 0) {
            this.orders.push({
                products: this.cart.contents,
                totalAmount: this.cart.totalAmount
            })
        }
        return this;
    }
}

class Cart {
    constructor() {
        this.contents = [];
        this.totalAmount = 0;
    }
    addToCart(product, quantity) {
        this.contents.push({
            product: product,
            quantity: quantity
        })
        return this;
    }
    showCartContents() {
        console.log(this.contents);
        return this;
    }
    updateProductQuantity(name, quantity) {
        this.contents.forEach(product => {
            if (product.product.name === name) {
                product.quantity = quantity;
            }
        })
        return this;
    }
    clearCartContents() {
        this.contents = [];
        return this;
    }
    computeTotal() {
        this.totalAmount = this.contents.reduce((acc, product) => {
            return acc + product.product.price * product.quantity;
        }, 0)
        return this;
    }
}


class Product {
    constructor(name, price) {
        this.name = name;
        this.price = price;
        this.isActive = true;
    }
    archive() {
        if (this.isActive) {
            this.isActive = false;
        }
        return this;
    }
    updatePrice(price) {
        this.price = price;
        return this;
    }
}

// Check Output: Customer creation
const john = new Customer('john@mail.com');
console.log(john);

// Check Output: Product creation
const prodA= new Product('soap', 9.99);
console.log(prodA);

// // Check Output: Product update price
// prodA.updatePrice(12.99);
// console.log(prodA);

// // Check Output: Product archive
prodA.archive();
// console.log(prodA);

// Check Output: Add to cart
john.cart.addToCart(prodA, 3);
// console.log(john.cart);

//Check Output: Show cart contents
john.cart.showCartContents();


// Check Output: Update product quantity in cart
// john.cart.addToCart(prodA, 3);
// john.cart.updateProductQuantity('soap', 5);
// console.log(john.cart);

// Check Output: Clear cart
john.cart.clearCartContents();
// console.log(john.cart);

// check output: Compute total
// john.cart.addToCart(prodA, 3);
// john.cart.computeTotal();
// console.log(john.cart);

// Check Output: Check out
john.cart.addToCart(prodA, 6);
john.cart.computeTotal();
john.checkOut();
console.log(john);
